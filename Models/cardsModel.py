import pickle
import base64
import sqlalchemy.exc
from sqlalchemy import func
from tables import HeroCard, LandCard, SpellCard, EquipmentCard, Session

session = Session()


def collection_stats():
    collection_stats = {'color': {}, 'rarity': {}, 'mixed': {}}
    collection_stats['total'] = session.query(func.count(HeroCard.id)).scalar()
    collection_stats['color'] = {f[0]: f[1] for f in session.query(HeroCard.color,
                                                                        func.count(HeroCard.id)).group_by(HeroCard.color)}
    collection_stats['rarity'] = {f[0]: f[1] for f in session.query(HeroCard.rarity,
                                                                         func.count(HeroCard.id)).group_by(HeroCard.rarity)}
    collection_stats['mixed'] = {f[0]+'-'+f[1]: f[2] for f in session.query(HeroCard.color,
                                                                               HeroCard.rarity,
                                                                               func.count(HeroCard.id)).group_by(HeroCard.color,
                                                                                                                 HeroCard.rarity)}
    Session.remove()
    return collection_stats


class Hero():
    def get(self, **args):
        lands = []
        limit = 1000
        offset = 0
        query = session.query(HeroCard.data)
        if 'limit' in args.keys():
            limit = int(args['limit'])
            del args['limit']
        if 'offset' in args.keys():
            offset = int(args['offset'])
            del args['offset']
        query = query.filter_by(**args)
        query = query.limit(limit)
        query = query.offset(offset)
        for card_data in query.all():
            land_card = pickle.loads(base64.b64decode(card_data.data))
            lands.append(land_card)
        Session.remove()
        return lands

    def update(self, key, **args_dict):
        num = session.query(HeroCard).filter_by(id=key).update(args_dict)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def create(self, hero):
        hero = HeroCard(hero.id, hero.color, hero.rarity, hero.serializedCard())
        session.add(hero)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def delete(self, key):
        hero_card = session.query(HeroCard).get(key)
        session.delete(hero_card)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True


# Land Related
class Land():
    def get(self, **args):
        lands = []
        limit = 1000
        offset = 0
        query = session.query(LandCard.data)
        if 'limit' in args.keys():
            limit = int(args['limit'])
            del args['limit']
        if 'offset' in args.keys():
            offset = int(args['offset'])
            del args['offset']
        query = query.filter_by(**args)
        query = query.limit(limit)
        query = query.offset(offset)
        for card_data in query.all():
            land_card = pickle.loads(base64.b64decode(card_data.data))
            lands.append(land_card)
        Session.remove()
        return lands

    def update(self, key, **args_dict):
        num = session.query(LandCard).filter_by(id=key).update(args_dict)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def create(self, land):
        land = LandCard(land.name, land.rarity, land.serializedCard())
        session.add(land)
        try:
            session.flush()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def delete(self, key):
        land_card = session.query(LandCard).get(key)
        session.delete(land_card)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

# Spell Related
class Spell():
    def get(self, **args):
        lands = []
        limit = 1000
        offset = 0
        query = session.query(SpellCard.data)
        if 'limit' in args.keys():
            limit = int(args['limit'])
            del args['limit']
        if 'offset' in args.keys():
            offset = int(args['offset'])
            del args['offset']
        query = query.filter_by(**args)
        query = query.limit(limit)
        query = query.offset(offset)
        for card_data in query.all():
            land_card = pickle.loads(base64.b64decode(card_data.data))
            lands.append(land_card)
        Session.remove()
        return lands

    def update(self, key, **args_dict):
        num = session.query(SpellCard).filter_by(id=key).update(args_dict)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def create(self, card):
        card = SpellCard(card.name, card.rarity,
                         card.cast_type, card.color,
                         card.serializedCard())
        session.add(card)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def delete(self, key):
        land_card = session.query(SpellCard).get(key)
        session.delete(land_card)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

# Equipment Related
class Equipment():
    def get(self, **args):
        lands = []
        limit = 1000
        offset = 0
        query = session.query(EquipmentCard.data)
        if 'limit' in args.keys():
            limit = int(args['limit'])
            del args['limit']
        if 'offset' in args.keys():
            offset = int(args['offset'])
            del args['offset']
        query = query.filter_by(**args)
        query = query.limit(limit)
        query = query.offset(offset)
        for card_data in query.all():
            land_card = pickle.loads(base64.b64decode(card_data.data))
            lands.append(land_card)
        Session.remove()
        return lands

    def update(self, key, **args_dict):
        num = session.query(EquipmentCard).filter_by(id=key).update(args_dict)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def create(self, card):
        card = EquipmentCard(card.name, card.rarity,
                             card.color,
                             card.serializedCard())
        session.add(card)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def delete(self, key):
        land_card = session.query(EquipmentCard).get(key)
        session.delete(land_card)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True
