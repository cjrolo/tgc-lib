from wtforms import Form, BooleanField, TextField, \
                    validators, SelectField,\
                    TextAreaField, SelectMultipleField, IntegerField
import card


class BaseForm(Form):
    rarity_list = []
    for rarity in card.card_rarity:
        rarity_list.append((rarity,rarity))
    name = TextField('Name', [validators.Length(min=3, max=20), validators.required()])
    rarity = SelectField('Rarity', [validators.required()],choices=rarity_list)


class NewLand(BaseForm):
    text = TextAreaField('Text', [validators.required()])
    lore = TextAreaField('Lore')


class NewHero(BaseForm):
    color_list = []
    for color in card.card_type:
        color_list.append((color,color))
    active_list = []
    for active in card.active_skills:
        active_list.append((active,active))
    passive_list = []
    for passive in card.passive_skills:
        passive_list.append((passive,passive))
    cost = IntegerField('Cost', [validators.InputRequired()])
    color = SelectField('Color', [validators.required()], choices=color_list)
    passives = SelectMultipleField('Passives',choices=passive_list)
    actives = SelectMultipleField('Actives',choices=active_list)
    attack = IntegerField('Attack', [validators.InputRequired()])
    hp = IntegerField('HP', [validators.required()])
    # For Promotion purposes
    promotion_cost = IntegerField('Promotion Cost', [validators.required()])
    promotion_attack = IntegerField('Promotion Attack', [validators.required()])
    promotion_hp = IntegerField('Promotion HP', [validators.required()])
    promotion_passives = SelectMultipleField('Promotion Passives',choices=passive_list)
    promotion_actives = SelectMultipleField('Promotion Actives',choices=active_list)
    # For Other stuff
    text = TextAreaField('Text')
    lore = TextAreaField('Lore')


class NewEquipment(BaseForm):
    color_list = []
    for color in card.card_type:
        color_list.append((color,color))
    cost = IntegerField('Cost', [validators.required()])
    color = SelectField('Color', [validators.required()], choices=color_list)
    damage = IntegerField('Extra Damage', [validators.InputRequired()], default=0)
    shield = IntegerField('Shield Value', [validators.InputRequired()], default=0)
    durability = IntegerField('Durability', [validators.InputRequired()], default=0)
    text = TextAreaField('Text')
    lore = TextAreaField('Lore')


class NewSpell(BaseForm):
    spell_list = []
    color_list = []
    for color in card.card_type:
        color_list.append((color,color))
    for spell_type in card.spell_cast_type:
        spell_list.append((spell_type,spell_type))
    cost = IntegerField('Cost', [validators.InputRequired()])
    color = SelectField('Color', [validators.required()], choices=color_list)
    cast_type = SelectField('Cast Type', [validators.required()], choices=spell_list)
    text = TextAreaField('Text')
    lore = TextAreaField('Lore')


class NewCollection(Form):
    name = TextField('Name', [validators.Length(min=3, max=20), validators.required()])
    type = SelectField('Type', [validators.required()], choices=[('Deck','Deck'),
                                                                 ('Collection', 'Collection')])
    comment = TextAreaField('Comment')