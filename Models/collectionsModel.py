from tables import CollectionsStore, CollectionsBase, Session
import sqlalchemy.exc

session = Session()


class BaseCollection():
    def __init__(self):
        self.type = ''

    def create(self, name, comment, owner=0):
        # Owner = 0 means its public
        collection = CollectionsBase(**{'owner_id': owner,
                                      'type': self.type,
                                      'name': name,
                                      'comment': comment})
        session.add(collection)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
        return False

    def update(self, key, name, comment):
        num = session.query(CollectionsBase).filter_by(id=key).update(name=name, comment=comment)
        print(num)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def get(self, **args):
        collections = []
        limit = 100
        offset = 0
        query = session.query(CollectionsBase)
        if 'limit' in args.keys():
            limit = int(args['limit'])
            del args['limit']
        if 'offset' in args.keys():
            offset = int(args['offset'])
            del args['offset']
        query = query.filter_by(**args)
        query = query.limit(limit)
        query = query.offset(offset)
        for collection in query.all():
            collections.append(collection)
        Session.remove()
        return collections

    def delete(self, key):
        collection = session.query(CollectionsBase).get(key)
        session.delete(collection)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError:
            session.rollback()
            Session.remove()
            return False
        Session.remove()
        return True

    def getCards(self, collection_id, **args):
        cards = []
        limit = 100
        offset = 0
        query = session.query(CollectionsStore, id=collection_id)
        if 'limit' in args.keys():
            limit = int(args['limit'])
            del args['limit']
        if 'offset' in args.keys():
            offset = int(args['offset'])
            del args['offset']
        query = query.filter_by(**args)
        query = query.limit(limit)
        query = query.offset(offset)
        for card in query.all():
            cards.append(card)
        Session.remove()
        return cards


class Deck(BaseCollection):
    def __init__(self):
        self.type = 'Deck'


class Collection(BaseCollection):
    def __init__(self):
        self.type = 'Collection'
