from wtforms import Form, TextField, validators, SelectField, TextAreaField


class CollectionForm(Form):
    name = TextField('Name', [validators.Length(min=3, max=20), validators.required()])
    type = SelectField('Type', [validators.required()], choices=['Deck', 'Collection'])
    comment = TextAreaField('Comment')

