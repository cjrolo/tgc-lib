from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, CHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
import os
from sqlalchemy.orm import sessionmaker, scoped_session

engine = create_engine('sqlite:///'+os.path.abspath(os.path.join(os.path.dirname(__file__), '.')) + '/../Data/card_sqllite.db', echo=True)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)

Base = declarative_base()


class HeroCard(Base):

    __tablename__ = "heroes"
    id = Column(CHAR(32), primary_key=True, autoincrement=True)
    color = Column(String, index=True)
    rarity = Column(String)
    data = Column(String)

    def __init__(self, id, color, rarity, data):
        self.id = id
        self.color = color
        self.rarity = rarity
        self.data = data

class SpellCard(Base):

    __tablename__ = "spells"
    id = Column(CHAR(32), primary_key=True, autoincrement=True)
    color = Column(String, index=True)
    cast_type = Column(String)
    rarity = Column(String)
    data = Column(String)

    def __init__(self, id, color, rarity, cast_type, data):
        self.id = id
        self.color = color
        self.rarity = rarity
        self.cast_type = cast_type
        self.data = data

class LandCard(Base):

    __tablename__ = "lands"
    id = Column(CHAR(32), primary_key=True, autoincrement=True)
    rarity = Column(String, index=True)
    data = Column(String)

    def __init__(self, id, rarity, data):
        self.id = id
        self.rarity = rarity
        self.data = data

class EquipmentCard(Base):

    __tablename__ = "equipments"
    id = Column(CHAR(32), primary_key=True, autoincrement=True)
    color = Column(String, index=True)
    rarity = Column(String)
    data = Column(String)

    def __init__(self, id, color, rarity, data):
        self.id = id
        self.color = color
        self.rarity = rarity
        self.data = data


class CollectionsBase(Base):

    __tablename__ = "collections"
    id = Column(Integer, primary_key=True, autoincrement=True)
    owner_id = Column(String, index=True)
    type = Column(String)
    name = Column(String)
    comment = Column(String)
    """
    def __init__(self, owner_id, type, name, comment):
        self.owner_id = owner_id
        self.name = name
        self.type = type
        self.comment = comment
    """

class CollectionsStore(Base):

    __tablename__ = "collections_store"
    id = Column(Integer, primary_key=True, autoincrement=True)
    collection_id = Column(CHAR(32), index=True)
    card_id = Column(CHAR(32))
    card_quantity = Column(Integer)


def bootstrap_database():
    import os
    file_path = os.path.abspath(os.path.join(os.path.dirname(__file__))) + '/../Data/'
    file = 'card_sqllite.db'
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    # Truncate and open
    f = open(file_path + file, 'w+')
    f.close()
    engine = create_engine('sqlite:///' + file_path + file)
    Base.metadata.create_all(engine)