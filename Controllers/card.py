from uuid import uuid1
import pickle
import base64

"""
This file defines all the Cards
All the effects the cards can have
All the types
etc...
"""
# Card Types, Rarity, etc...
card_type = ['Charisma', 'Strength', 'Agility', 'Constitution', 'Intelligence']
card_rarity = ['Legendary', 'Rare', 'Uncommon', 'Common']
passive_skills = ['Shield', 'Taunt', 'Leadership', 'Fear']
active_skills = ['Movement', 'Silence', 'Protect']
active_affinity = {'Charisma': ['Protect'],
                   'Strength': ['Taunt'],
                   'Agility': ['Movement'],
                   'Constitution': ['Taunt'],
                   'Intelligence': ['Silence']}
passive_affinity = {'Charisma': ['Leadership'],
                    'Strength': ['Boost'],
                    'Agility': ['Hover'],
                    'Constitution': ['Shield'],
                    'Intelligence': ['Fly']}
target_types = ['player', 'hero', 'land', 'enchantment', 'permanent', 'everything', 'self']
target_types_modifiers = ['you', 'enemy','none'
                          'any', 'any-your', 'any-enemy'
                          'all', 'all-your', 'all-enemy']
spell_cast_type = ['Tactic', 'Strategy']
effects = []


class BaseCard():
    def serializedCard(self):
        return base64.b64encode(pickle.dumps(self))

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        setattr(self, key, value)

    def __delitem__(self, key):
        delattr(self, key)

    def toXML(self):
        pass

    def toCSV(self):
        pass


class LandCard(BaseCard):

    def __init__(self, rarity='Common', name=uuid1(), text='', lore=''):
        self.name = name
        self.rarity = rarity
        self.text=text
        self.lore=lore


class EquipmentCard(BaseCard):

    def __init__(self, color='Charisma', durability=1, shield=0, damage=0, cost=0, rarity='Common', name=uuid1(), text='', lore=''):
        self.name = name
        self.color = color
        self.rarity = rarity
        self.durability = durability
        self.passive = []
        self.active = []
        self.cost = cost
        self.damage = damage
        self.shield = shield
        self.text = text
        self.lore = lore

    def toXML(self):
        pass


class SpellCard(BaseCard):

    def __init__(self, color='Charisma', cast_type='Tactic', cost=0, rarity='Common', name=uuid1(), text='', lore=''):
        self.name = name
        self.rarity = rarity
        self.cost = cost
        self.cast_type = cast_type
        self.color = color
        self.text = text
        self.lore = lore

    def toXML(self):
        pass


class HeroCard(BaseCard):

    def __init__(self, color='Charisma', cost=0, rarity='Common', name=uuid1(), text='', lore='', passives=[],
                 actives=[], attack=0, hp=1, promotion_hp=0, promotion_attack=0, promotion_passives=[],
                 promotion_actives=[], promotion_cost=1):
        self.id = str(uuid1())
        self.name = str(name)
        self.rarity = rarity
        self.generate_promotion = False
        self.color = color
        self.passives = passives
        self.actives = actives
        self.cost = cost
        self.attack = attack
        self.hp = hp
        self.text = text
        self.lore = lore
        self.promotion_passives = promotion_passives
        self.promotion_actives = promotion_actives
        self.promotion_cost = promotion_cost
        self.promotion_hp = promotion_hp
        self.promotion_attack = promotion_attack

    def __len__(self):
        return 10

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        setattr(self, key, value)

    def __delitem__(self, key):
        delattr(self, key)

    def __iter__(self):
        pass

    def __contains__(self, item):
        pass

    def __repr__(self):
        output_str = '(%s) Cost: %s \t %s/%s \n' % (self.card_type, self.cost, self.attack, self.hp)
        passive_str = 'Passives: \n'
        for passive in self.passive:
            passive_str += '\t%s\n' % passive
        active_str = 'Active: \n'
        for active in self.active:
            active_str += '\t%s\n' % active
        promotion_str = 'Promotion (%s): ' % str(self.promotion['cost'])
        promotion_str += '\t+%s/+%s\n' % ( self.promotion['attack'],  self.promotion['hp'] )
        promotion_str += '\tPassives: '
        for passive in self.promotion['passive']:
            promotion_str += '%s, ' % passive
        promotion_str += '\n\tActive: '
        for active in self.promotion['active']:
            promotion_str += '%s, ' % active
        return ''.join([output_str, active_str, passive_str, promotion_str])

    def toXML(self):
        """
        Output a XML
        :return:
        """
        xml = "<?xml version='1.0' encoding='utf-8'?>\n"
        xml += "<card xmlns='http://www.w3.org/2005/Atom' xml:lang='en'>\n"
        xml += "\t<type>Hero</type>\n"
        xml += "\t<color>%s</color>\n" % self.card_type
        xml += "\t<cost>%s</cost>\n" % self.cost
        xml += "\t<hp>%s</hp>\n" % self.hp
        xml += "\t<attack>%s</attack>\n" % self.attack
        xml += "\t<name>%s</name>\n" % self.name
        xml += "\t<rarity>%s</rarity>\n" % self.rarity
        if len(self.active) > 0:
            for active in self.active:
                xml += "\t<active>%s</active>\n" % active
        if len(self.passive) > 0:
            for passive in self.passive:
                xml += "\t<passive>%s</passive>\n" % passive
        xml += "\t<promotion>\n"
        xml += "\t\t<cost>%s</cost>\n" % self.promotion['cost']
        xml += "\t\t<hp>%s</hp>\n" % self.promotion['hp']
        xml += "\t\t<attack>%s</attack>\n" % self.promotion['attack']
        if len(self.promotion['active']) > 0:
            for active in self.promotion['active']:
                xml += "\t\t<active>%s</active>\n" % active
        if len(self.promotion['passive']) > 0:
            for passive in self.promotion['passive']:
                xml += "\t\t<passive>%s</passive>\n" % passive
        xml += "\t</promotion>\n"
        xml += "</card>"
        return xml

    def togglePromotion(self):
        self.generate_promotion = True

    def setAbility(self, ability_type, ability):
        if ability_type == 'passive':
            self.passives.append(ability)
            return
        elif ability_type == 'active':
            self.actives.append(ability)
            return
        return