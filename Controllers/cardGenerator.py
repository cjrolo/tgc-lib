import random
import card
from card import passive_skills as passive, active_skills as active
from card import card_type, active_affinity, passive_affinity

"""
All Mambo Jambo magic stuff about creating cards is here!
This code will make your eyes bleed!
"""

lists = {'active': active, 'passive': passive}

# Hero Budgets
max_budget = 15
HP_budget = 0.55
attack_budget = 0.75
active_budget = 1
passive_budget = 1.5
max_HP = 15
max_attack = 15
cost_budget = -0.8


def returnElement(selectorType):
    """
        returns a random element from the selected Attribute
    """
    return random.choice(selectorType)



selector = {'passive': [passive_budget, returnElement],
            'active': [active_budget, returnElement],
            'cost': [cost_budget, 1],
            'hp': [HP_budget, 1],
            'attack': [attack_budget, 1]}

properties_enum = list(enumerate(['passive', 'active','hp','attack']))


def get_ability(type_of_ability, card_type=None):
    if card_type:
        ability = None
        random_number = random.randint(0, 9)
        if type_of_ability == 'passive' and random_number < 3:
            r = random.randint(0, len(passive_affinity[card_type])-1)
            return passive_affinity[card_type][r]
        elif type_of_ability == 'active' and random_number < 4:
            r = random.randint(0,len(active_affinity[card_type])-1)
            return active_affinity[card_type][r]
    return returnElement({'passive': passive, 'active': active}[type_of_ability])

def generate_promotion(budget, order):
    attack = int(random.lognormvariate(1, 0.6))
    hp = int(random.lognormvariate(1, 0.7))
    if hp > max_HP:
        hp = max_HP
    if attack > max_attack:
        attack = max_attack
    passive = []
    if random.randint(0, 99) < 10:
        passive = [get_ability('passive')]
    active = []
    if random.randint(0, 99) < 5:
        active = [get_ability('active')]
    # Avoid empty promotions
    generated = {'attack': [attack, attack*attack_budget],
                 'hp': [hp, hp*HP_budget],
                 'passive': [passive, passive_budget],
                 'active': [active, active_budget]}
    out = {'hp': 1}
    for element in order:
        name = properties_enum[element][1]
        out[name] = generated[name][0]
        budget -= generated[name][1]
        if budget < 0:
            break
    if budget < 0:
        out['cost'] = abs(int(budget/cost_budget))+1
    return out

def generate_hero_card(card_type):
    """
    Rules to generate a card
    1) First Generate Color (it's given, so no worry)
    2) Try to generate a active Ability (15% Change)
    3) Try to generate a passive Ability (40% if no Active, 10% if Active)
    4) Try to generate more passive (Probability drops by half for each one generated)
    5) Generate Attack
    6) Generate HP
    7) Use cost to balance budget
    8) Create Promotion Budget
    :param card_type: The card Color
    :return: The generated card
    """
    total_budget = 0
    # Budget should be distribute in a Gaussian way, not uniform
    available_budget = int(random.gauss(2, 1))+1
    total_budget += available_budget
    #print("Available Card Budget: %s"  % available_budget)
    budget = 0
    passive_probability = 40
    # Step 1
    new_card = card.HeroCard(card_type)
    # Step 2
    if random.randint(0, 99) < 15:
        passive_probability = 10
        budget += active_budget
        new_card.setAbility('active', get_ability('active', card_type))
    # Step 3
    if random.randint(0, 99) < passive_probability:
        budget += passive_budget
        new_card.setAbility('passive', get_ability('passive', card_type))
        passive_probability /= 2
        # Step 4
        while passive_probability > 1:
            if random.randint(0, 99) < passive_probability:
                budget += passive_budget
                new_card.setAbility('passive', get_ability('passive'))
                passive_probability /= 2
            else:
                break
    #weight_list = weights_sets[random.randint(0, len(weights_sets) - 1)]
    # Step 5 and 6
    attack = int(random.lognormvariate(1, 0.6))
    hp = int(random.lognormvariate(1, 0.7)) + random.randint(0,3)
    if hp > max_HP:
        hp = max_HP
    if attack > max_attack:
        attack = max_attack
    if hp == 0:
        hp = 1
    new_card['attack'] = attack
    new_card['hp'] = hp
    budget += hp*HP_budget
    budget += attack*attack_budget
    # Step 7
    new_card['cost'] += abs(int((budget - available_budget)*cost_budget))
    total_budget += budget
    #print("Used Card Budget: %s"  % budget)
    # Step 8
    available_budget = int(random.gauss(2, 0.5))+1
    total_budget += available_budget
    #print("Promotion Budget: %s"  % available_budget)
    promotion = generate_promotion(available_budget, random.sample(range(4), 4))
    new_card['promotion'] = promotion
    total_budget = int(total_budget)
    if total_budget < 6 or total_budget > 13:
        new_card['rarity'] = 'Rare'
    elif total_budget in [6, 7, 12, 13]:
        new_card['rarity'] = 'Uncommon'
    else:
        new_card['rarity'] = 'Common'
    return new_card

def generate_collection(total=90, color=None):
    """

    :param total: Total Cards generated, if no color is selected, will generate equal total/5 cards for each color
    :param color: Type in if you want to generate a single color. It will generate total cards of this color
    :return:
    """
    card_collection = []
    if not color:
        for color in card_type:
            for i in range(int(total/4)):
                generated_card = generate_hero_card(color)
                card_collection.append(generated_card)
    else:
        for i in range(total):
            generated_card = generate_hero_card(color)
            card_collection.append(generated_card)
    return card_collection