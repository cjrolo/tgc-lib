#!/usr/bin/python3.3
from time import sleep
import urllib.request
from bs4 import BeautifulSoup
from string import Template
import base64

abilities = ['Aggression', 'Anti-air', 'Backstab', 'Berserk', 'Break', 'Cautious', 'Chance',
             'Charge', 'Cleave', 'Counter', 'Courage', 'Critical', 'Crystal', 'Deathstrike', 'Demoralize',
             'Destroy', 'Disarm', 'Disease', 'Doom', 'Drain Crystal', 'Entangle', 'Explode', 'Fear',
             'Flying', 'Fortify', 'Heal', 'Heal all', 'Immunity', 'Invincibility', 'Opportunity', 'Paint',
             'Reach', 'Reflect', 'Regenerate', 'Resonate', 'Revive', 'Shield', 'Silence', 'Stealth',
             'Strike', 'Strike*', 'Stun', 'Thorn', 'Thrash', 'Trample', 'Unsummon', 'Wall']

card_type = ['Armor', 'Magic', 'Melee', 'Range']

url_type = {'Monster': 'http://www.edgebee.com/wiki/index.php?title=Category:Monster_Cards',
            'Equipment': 'http://www.edgebee.com/wiki/index.php?title=Category:Equipment_Cards',
            }

url_color = {'Red': 'http://www.edgebee.com/wiki/index.php?title=Category:War_Cards',
             'Yellow': 'http://www.edgebee.com/wiki/index.php?title=Category:Fortune_Cards',
             'Blue': 'http://www.edgebee.com/wiki/index.php?title=Category:Balance_Cards',
             'Green': 'http://www.edgebee.com/wiki/index.php?title=Category:Nature_Cards',
             'Purple': 'http://www.edgebee.com/wiki/index.php?title=Category:Chaos_Cards',
             'Grey': 'http://www.edgebee.com/wiki/index.php?title=Category:Generic_Cards'
}

url_rarity = {'Common': 'http://www.edgebee.com/wiki/index.php?title=Category:Common_Cards',
              'Uncommon': 'http://www.edgebee.com/wiki/index.php?title=Category:Uncommon_Cards',
              'Rare': 'http://www.edgebee.com/wiki/index.php?title=Category:Rare_Cards',
              'Epic': 'http://www.edgebee.com/wiki/index.php?title=Category:Epic_Cards'
}

url_expansion = {'Original': 'http://www.edgebee.com/wiki/index.php?title=Category:Original_Cards',
                 'Exp 1': 'http://www.edgebee.com/wiki/index.php?title=Category:Expansion_1_Cards',
                 'Exp 2': 'http://www.edgebee.com/wiki/index.php?title=Category:Expansion_2_Cards',
                 'Exp 3': 'http://www.edgebee.com/wiki/index.php?title=Category:Expansion_3_Cards',
                 'Exp 4': 'http://www.edgebee.com/wiki/index.php?title=Category:Expansion_4_Cards',
                 }

urls = {
    #'type': url_type,
    'color': url_color,
    'rarity': url_rarity,
    'exp': url_expansion
}

# Making Pretty stuff
color_equivalences = {"Grey": "label-default",
                      "Red": "label-important",
                      "Blue": "label-info",
                      "Yellow": "label-warning",
                      "Purple": "label-purple",
                      "Green": "label-success",
                    }

badge_equivalences = {"Epic": "badge-important",
                      "Rare": "badge-warning",
                      "Uncommon": "badge-info",
                      "Common": "badge-white",
                      "": ""
                      }


# The name says it all right? :)
def retrieveCardImage(html, **kargs):
    try:
        #width="800"
        lista = str(html).split('<td width="800"', 1)[1].split('</td>')[0]
    except:
        lista = str(html).split('<table ', 1)[1].split('</tr></table>')[0]
    soup = BeautifulSoup(lista)
    db = kargs['db']
    card_name = kargs['name']
    for tag in soup.findAll('img'):
        if str(tag.get('src')).find('.png') != -1:
            db.updateItem(card_name, 'img', tag.get('src'))


def retrieveCardAbilities(html, **kargs):
    lista = str(html).split('<div id="mw-normal-catlinks">', 1)[1].split('</div>')[0]
    soup = BeautifulSoup(lista)
    db = kargs['db']
    card_name = kargs['name']
    for tag in soup.findAll('a'):
        if tag.text[:-6] in abilities:
            db.updateItem(card_name, 'abilities', tag.text[:-6])

def retrieveCardAttackType(html, **kargs):
    lista = str(html).split('<div id="mw-normal-catlinks">', 1)[1].split('</div>')[0]
    soup = BeautifulSoup(lista)
    db = kargs['db']
    card_name = kargs['name']
    for tag in soup.findAll('a'):
        if tag.text[:-6] in card_type:
            db.updateItem(card_name, 'card_type', tag.text[:-6])


def retrieveCardRarity(html, **kargs):
    lista = str(html).split('<div id="mw-normal-catlinks">', 1)[1].split('</div>')[0]
    soup = BeautifulSoup(lista)
    db = kargs['db']
    card_name = kargs['name']
    for tag in soup.findAll('a'):
        if tag.text[:-6] in url_rarity.keys():
            db.updateItem(card_name, 'rarity', tag.text[:-6])


def retrieveCardMaxStats(html, **kargs):
    soup = BeautifulSoup(str(html))
    db = kargs['db']
    card_name = kargs['name']
    # 7 Rows, 6 Columns, only the 6th is interesting, and the first and last row are to be ignored
    current_row = 0
    # Data Order: Cost, HP, Melee, Range, Magic;
    card_values = []
    i = 0
    if db.dataHolder[card_name]['exp'] == 'Exp 4':
        i = 1
    for row in soup.findAll('table')[0].findAll('tr'):
        if current_row in [0, i, i + 1, i + 2] or current_row > 7 + i:
            current_row += 1
            continue
        current_row += 1
        card_values.append(row.findAll('td')[5].text.strip(' \\n'))
    db.updateItem(card_name, 'stats', ' ,'.join(card_values))


def updateCardInformation(html, **kargs):
    retrieveCardImage(html, **kargs)
    retrieveCardAbilities(html, **kargs)
    retrieveCardRarity(html, **kargs)
    retrieveCardAttackType(html, **kargs)
    if kargs['db'].dataHolder[kargs['name']]['type'] == 'Monster':
        retrieveCardMaxStats(html, **kargs)

# Cache Functions

class fetchContent():

    def __init__(self, cache_path='cenas_python/cache/', process_func=None):
        self._cache_path = cache_path
        self._processFunc = process_func

    def fetch(self, url, process=False, force_retrieve=False, **kargs):
        if not force_retrieve:
            html = self._getCache(url)
        if  force_retrieve or not html:
            print('Requesting URL %s' % url)
            try:
                response = urllib.request.urlopen(url)
                html = response.read()
                # Avoiding massive requests
                sleep(2)
                self._generateCache(url, html)
                if process and self._processFunc:
                    return self._processFunc(html, **kargs)
            except:
                return None
        return html

    def _generateCache(self, url, content):
        print('Creating Cache!')
        hashed = base64.urlsafe_b64encode(url.encode('ascii'))
        f = open(self._cache_path+str(hashed), 'w')
        f.write(str(content))
        f.close()

    def _getCache(self, url):
        try:
            hashed = base64.urlsafe_b64encode(url.encode('ascii'))
            f = open(self._cache_path+str(hashed), 'r')
            content = f.read()
            f.close()
            print('Cache Available!')
            return content
        except:
            return


# Card Structure
def Card(name, type='', color='Grey', exp='Original', src='', rarity='', *attr):
    return {'name': name,
            'color': color,
            'url': "href='http://www.edgebee.com"+src+"'",
            'exp': exp,
            'rarity': rarity,
            'type': type,
            'card_type': '',
            'stats': 'N/A',
            'abilities': ''}


# Database
class InternalDatabase():

    def __init__(self, engine={}):
        self.dataHolder = engine
        self.cards = {'Monster': [],
                      'Equipment': []}

    def addItem(self, data):
        if data[2] == '':
            return
        if data[0] in self.cards[data[2]]:
            return
        self.cards[data[2]].append(data[0])
        self.dataHolder[data[0]] = Card(data[0], src=data[1], type=data[2])

    def saveDB(self):
        with open('cenas_python/cardsDB.db', 'w') as f:
            for k in self.dataHolder.keys():
                # Name, Color, Exp, URL
                line = ''
                for elem in self.dataHolder[k].keys():
                    line += self.dataHolder[k][elem]+', '
                f.write(line[:-2] + "\n")
            f.close()

    def updateItem(self, item_key, property_key, value):
        try:
            if property_key == 'abilities':
                self.dataHolder[item_key]['abilities'] += ', %s' % value
            # Hack for multi-colored Cards
            elif (property_key == 'color' and
                  self.dataHolder[item_key]['color'] != 'Grey' and
                  value != 'Grey'):
                self.dataHolder[item_key]['color'] += ' ' + value
            elif property_key == 'card_type':
                self.dataHolder[item_key]['card_type'] += ', %s' % value
            else:
                self.dataHolder[item_key][property_key] = value
        except:
            print('Card Not Found: %s' % item_key)
            self.addItem([item_key, '', 'Equipment', ])
            return 1

    def dumpDB(self):
        pass

    def restoreDB(self):
        pass

# Generator

def generateHTML(data):
    with open('cenas_python/template.html', 'r') as f:
        template = f.read()
    f.close()
    s = Template(template)
    output = s.safe_substitute(data)
    with open('cenas_python/default.html', 'w') as file:
        file.write(output)
    file.close()

def generateLines(db, type='all'):
    lines = ''
    with open('cenas_python/table_template.html', 'r') as f:
        template = f.read()
    f.close()
    s = Template(template)
    if type == 'all':
        for k in db.dataHolder.keys():
            db.dataHolder[k]["colorCell"] = ''
            for cor in db.dataHolder[k]["color"].split(' '):
                db.dataHolder[k]["colorCell"] += '<span class="label %s">%s</span>' % (color_equivalences[cor],
                                                                                       cor)
            db.dataHolder[k]["badgeColor"] = badge_equivalences[db.dataHolder[k]["rarity"]]
            db.dataHolder[k]["expansionName"] = "<td>" + db.dataHolder[k]["exp"] + "</td>"
            db.dataHolder[k]["cardAbilities"] = "<td>" + db.dataHolder[k]["abilities"][1:] + "</td>"
            db.dataHolder[k]["attackType"] = "<td>" + db.dataHolder[k]["card_type"][1:] + "</td>"
            db.dataHolder[k]["maxLvlStats"] = "<td>" + db.dataHolder[k]["stats"] + "</td>"
            lines += s.safe_substitute(db.dataHolder[k])
        return lines
    elif type == 'Equipment':
        for k in db.cards[type]:
            db.dataHolder[k]["colorCell"] = ''
            for cor in db.dataHolder[k]["color"].split(' '):
                db.dataHolder[k]["colorCell"] += '<span class="label %s">%s</span>' % (color_equivalences[cor],
                                                                                       cor)
            db.dataHolder[k]["badgeColor"] = badge_equivalences[db.dataHolder[k]["rarity"]]
            db.dataHolder[k]["expansionName"] = "<td>" + db.dataHolder[k]["exp"] + "</td>"
            db.dataHolder[k]["attackType"] = "<td>" + db.dataHolder[k]["card_type"][1:] + "</td>"
            db.dataHolder[k]["maxLvlStats"] = ""
            lines += s.safe_substitute(db.dataHolder[k])
        return lines
    elif type == 'Monster':
        for k in db.cards[type]:
            db.dataHolder[k]["colorCell"] = ''
            for cor in db.dataHolder[k]["color"].split(' '):
                db.dataHolder[k]["colorCell"] += '<span class="label %s">%s</span>' % (color_equivalences[cor],
                                                                                       cor)
            db.dataHolder[k]["badgeColor"] = badge_equivalences[db.dataHolder[k]["rarity"]]
            db.dataHolder[k]["expansionName"] = "<td>" + db.dataHolder[k]["exp"] + "</td>"
            db.dataHolder[k]["attackType"] = "<td>" + db.dataHolder[k]["card_type"][1:] + "</td>"
            db.dataHolder[k]["maxLvlStats"] = "<td>" + db.dataHolder[k]["stats"] + "</td>"
            lines += s.safe_substitute(db.dataHolder[k])
        return lines
    else:
        # Expansion Specific tables will be generated here
        for k in db.dataHolder.keys():
            db.dataHolder[k]["colorCell"] = ''
            if db.dataHolder[k]['exp'] == type:
                for cor in db.dataHolder[k]["color"].split(' '):
                    db.dataHolder[k]["colorCell"] += '<span class="label %s">%s</span>' % (color_equivalences[cor],
                                                                                           cor)
                db.dataHolder[k]["badgeColor"] = badge_equivalences[db.dataHolder[k]["rarity"]]
                db.dataHolder[k]["expansionName"] = ""
                db.dataHolder[k]["attackType"] = "<td>" + db.dataHolder[k]["card_type"][1:] + "</td>"
                db.dataHolder[k]["maxLvlStats"] = "<td>" + db.dataHolder[k]["stats"] + "</td>"
                lines += s.safe_substitute(db.dataHolder[k])
        return lines

#Initialize
db = InternalDatabase()
fetcher = fetchContent()

# Parser
for k in url_type.keys():
    if url_type[k] != '':
        html = fetcher.fetch(url_type[k], force_retrieve=False)
        lista = str(html).split('<div id="mw-pages">', 1)[1].split('</tr></table>')[0]
        soup = BeautifulSoup(lista)
        for tag in soup.findAll('a'):
            #Remove the " Card" part of the String
            # This will add all Creatures and Itens
            db.addItem([tag.get('title')[:-5], tag.get('href'), k])

for k in urls.keys():
    for key in urls[k].keys():
        html = fetcher.fetch(urls[k][key], force_retrieve=False)
        lista = str(html).split('<div id="mw-pages">', 1)[1].split('</tr></table>')[0]
        soup = BeautifulSoup(lista)
        for tag in soup.findAll('a'):
            #Remove the " Card" part of the String
            # This will add all Creatures and Itens
            if db.updateItem(tag.get('title')[:-5], k, key) == 1:
                # Will also update the URL
                db.updateItem(tag.get('title')[:-5], 'url', "href='http://www.edgebee.com" + tag.get('href') + "'")

# This will fetch all the Cards!! Watch out if you want to do enable a force_retrieve here!
for card in db.dataHolder.keys():
    html = fetcher.fetch(db.dataHolder[card]['url'][6:-1])
    if html:
        updateCardInformation(html, db=db, name=db.dataHolder[card]['name'])

db.saveDB()
generateHTML({'all': generateLines(db), 'monsters': generateLines(db, 'Monster'),
              'equipment': generateLines(db, 'Equipment'),
              'exp1': generateLines(db, 'Exp 1'),
              'exp2': generateLines(db, 'Exp 2'),
              'exp3': generateLines(db, 'Exp 3'),
              'exp4': generateLines(db, 'Exp 4')
              })
