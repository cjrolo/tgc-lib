import collectionsModel


class CollectionController():

    def __init__(self, type='Deck'):
        self.type = type
        self.model = {'Deck': collectionsModel.Deck,
                      'Collection': collectionsModel.Collection}[type]()

    def new(self, **args):
        """
        Entry point for creating a Land
        :return: a dictionary with the form values
        """
        self.model.create(args['name'], args['comment'])

    def update(self, key, **args):
        if 'owner' in args.keys():
            del args['owner']
        if 'type' in args.keys():
            del args['type']
        self.model.update(key, **args)

    def delete(self, key):
        self.model.delete(key)

    def list(self, **args):
        return self.model.get(**args)

    def getbyId(self, key):
        return self.model.get(id=key)[0]

collectionController = CollectionController()