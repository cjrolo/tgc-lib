import cardGenerator
import random
import cardsModel
from card import HeroCard, card_type


class heroController():

    def __init__(self):
        self.model = cardsModel.Hero()

    def createRandom(self, quantity=1):
        """
        Create a completely Random hero
        :return: list of Heroes Card
        """
        hero_list = []
        for i in range(int(quantity)):
            color = random.choice(card_type)
            new_hero = cardGenerator.generate_hero_card(color)
            self.model.create(new_hero)
            hero_list.append(new_hero)
        return hero_list

    def new(self, **args):
        """
        Entry point for creating a Hero
        :return: a dictionary with the form values
        """
        new_hero = HeroCard(**args)
        self.model.create(new_hero)

    def update(self, key, **args):
        if 'id' in args.keys():
            del args['id']
        card = self.getbyId(key)
        for k in args.keys():
            card[k] = args[k]
        args['data'] = card.serializedCard()
        if 'text' in args.keys():
            del args['text']
        if 'lore' in args.keys():
            del args['lore']
        self.model.update(key, **args)

    def delete(self, key):
        self.model.delete(key)

    def list(self, **args):
        return self.model.get(**args)

    def getbyId(self, key):
        return self.model.get(id=key)[0]

heroController = heroController()