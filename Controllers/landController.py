import cardGenerator
import random
import cardsModel
from card import LandCard, card_type

class LandController():

    def __init__(self):
        self.model = cardsModel.Land()

    def new(self, **args):
        """
        Entry point for creating a Land
        :return: a dictionary with the form values
        """
        new_land = LandCard(rarity=args['rarity'],
                            name=args['name'],
                            text=args['text'],
                            lore=args['lore'])
        self.model.create(new_land)

    def update(self, key, **args):
        if 'name' in args.keys():
            del args['name']
        card = self.getbyId(key)
        for k in args.keys():
            card[k] = args[k]
        args['data'] = card.serializedCard()
        if 'text' in args.keys():
            del args['text']
        if 'lore' in args.keys():
            del args['lore']
        self.model.update(key, **args)

    def delete(self, key):
        self.model.delete(key)

    def list(self, **args):
        return self.model.get(**args)

    def getbyId(self, key):
        return self.model.get(id=key)[0]

landController = LandController()