__author__ = 'Carlos Juzarte Rolo'

class CardManager:

    mandatory_fields = []

    def __init__(self, collection_id, collection_structure={}, mandatory_fields=[]):
        """
        Make sure all structures implements a dictionary like interface
        """
        self.base_structure = collection_structure
        self.mandatory_fields = mandatory_fields
        self.available_attributes = {}
        self.id = collection_id

    def _generateID(self, card):
        return card[self.mandatory_fields[0].replace(" ", "").lower()]

    def setMandatoryFields(self, *fields):
        # The first mandatory field will be the id of the card
        self.mandatory_fields = fields

    def addCard(self, card):
        # if the Card exists it will be overwritten by the new one
        id = self._generateID(card)
        self.base_structure[id] = card
        return id

    def updateCard(self, id, card):
        if not self.base_structure[id]:
            return None
        self.base_structure[id].update(card)

    def getCard(self, id):
        if not self.base_structure[id]:
            return None
        return self.base_structure[id]

    def getCardsByAttribute(self, attribute):
        if attribute not in self.available_attributes.keys():
            return None
        return self.available_attributes[attribute]

    def removeCard(self, id):
        del self.base_structure[id]

    def getCollectionReference(self):
        return self.id

    def getCollectionInfo(self):
        pass
