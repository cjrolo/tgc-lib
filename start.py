import tornado.ioloop
import tornado.web
import tornado.auth
import tornado.escape
import tornado.httpserver
import tornado.wsgi
import os
import sys
from tornado import gen
from bottle_mvc import bottle_app

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '.')) + '/Engine')

import comunication
"""
Web sockets will be used because a game is a long lasting session between
two players
"""

class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        user_json = self.get_secure_cookie("auth_user")
        if not user_json: return None
        return tornado.escape.json_decode(user_json)


class MainHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        name = tornado.escape.xhtml_escape(self.current_user["name"])
        self.write("Hello, " + name)
        self.write("<br><br><a href=\"/auth/logout\">Log out</a>")


class AuthHandler(BaseHandler, tornado.auth.GoogleMixin):
    @tornado.web.asynchronous
    @gen.coroutine
    def get(self):
        if self.get_argument("openid.mode", None):
            user = yield self.get_authenticated_user()
            self.set_secure_cookie("auth_user",
                                   tornado.escape.json_encode(user))
            self.redirect("/")
            return
        self.authenticate_redirect()


class LogoutHandler(BaseHandler):
    def get(self):
        # This logs the user out of this demo app, but does not log them
        # out of Google.  Since Google remembers previous authorizations,
        # returning to this app will log them back in immediately with no
        # interaction (unless they have separately logged out of Google in
        # the meantime).
        self.clear_cookie("auth_user")
        self.redirect("/")


class Application(tornado.web.Application):
    def __init__(self):
        bottle_handler = tornado.wsgi.WSGIContainer(bottle_app)
        handlers = [
            #(r"/", MainHandler),
            (r"/gamews", comunication.GameCommunication),
            (r"/auth/login", AuthHandler),
            (r"/auth/logout", LogoutHandler),
            (r".*", tornado.web.FallbackHandler, dict(fallback=bottle_handler)),
            ]
        settings = dict(
            cookie_secret="ThisIsAStrangeValueForAGeneratedRandomKey!99881297361?!",
            login_url="/auth/login",
            )
        tornado.web.Application.__init__(self, handlers, **settings)

if __name__ == "__main__":

    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(8081)
    tornado.ioloop.IOLoop.instance().start()