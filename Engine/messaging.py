import json

class Message():

    def __init__(self):
        self.number = 0
        self.message = ''
        self.payload = ''
        self.status = ''
        self.next = ''

    def toJSON(self):
        dict_rep = dict()
        for key, value in self.__dict__.iteritems():
            if not callable(value) and not key.startswith('__'):
                dict_rep[key] = value
        return json.dumps(dict_rep)

class MessageParser():

    valid_messages = ['search', 'create', 'start', 'concede', 'pass', 'play']

    def validateMessage(self, json_message):
        message = json.loads(json_message)
        try:
            if message["message"] not in self.valid_messages:
                return False
        except KeyError:
            return False
        return True
