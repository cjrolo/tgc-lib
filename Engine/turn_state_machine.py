class TurnStateMachine():

    phases = ['Start', 'Buying', 'Resources', 'Main', 'Discard']

    def __init__(self):
        current_phase = 0

    def changePhase(self):
        self.current_phase += 1
        if self.current_phase == len(self.phases):
            self.current_phase = 0