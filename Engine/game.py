import uuid
import random
from exceptions import InvalidPlay
from objects import get_card
from game_types import mana_types, card_types


class Game():

    def __init__(self, player, **settings):
        self.spectators = []
        self.players = [player]
        self.match_id = uuid.uuid1()
        self.match_settings = dict()
        self.match_settings['turn_limit'] = 0
        self.match_settings['time_limit'] = 0
        self.match_settings['players_hp'] = 25
        self.decks = {player: random.sample(range(250), 40)}
        self.gys = {player: []}
        self.lands = {player: []}
        self.creatures = {player: [None, None, None]}
        self.hands = {player: []}
        self.mana = {player: {'hp': 25, 'gen': 0, 'cha': 0, 'int': 0, 'agi': 0, 'con': 0, 'str': 0}}
        self.hp = {player: self.match_settings['players_hp']}
        self.current_player = None
        self.other_player = None

        if settings:
            self.match_settings = settings

    def gameState(self):
        print('Match ID: %s' % self.match_id)
        print('Players: %s, %s' % (self.players[0], self.players[1]))
        print('Current Turn: %s' % self.current_player)
        for i in [0, 1]:
            print('%s' % self.players[i])
            print('\tDeck: %s' % self.decks[self.players[i]])
            print('\tHand: %s' % self.hands[self.players[i]])
            print('\tGraveyard: %s' % self.gys[self.players[i]])
            print('\tBattlefield: %s' % self.lands[self.players[i]])
            print('\tHeroes: %s' % self.creatures[self.players[i]])
            print('\tResources: %s' % self.mana[self.players[i]])


    def start(self):
        count = 1
        random.shuffle(self.players)
        for player in self.players:
            if not self.other_player:
                self.other_player = player
            player.setGame(self)
            player.sendMessage({'message': 'start', 'payload': count})
            player.sendMessage({'message': 'set',
                                'payload': 'hp',
                                'target': 'all_players',
                                'quantity': self.match_settings['players_hp']})
            count -= 1
        self.current_player = player

    def _consumeResource(self, player, color, cost):
        if self.mana[player][color] > 0 and self.mana[player]['gen'] > cost:
            self.mana[player][color] -= 1
            self.mana[player]['gen'] -= cost
            return True
        return False

    def _increaseResource(self, target, resource, quantity):
        quantity = int(quantity)
        player = {'you': self.current_player,
                  'enemy': self.other_player}[target]
        if not resource in mana_types:
            raise InvalidPlay()
        self.mana[player][resource] += quantity
        for p in self.players:
            if p is player:
                p.sendMessage({'message': 'set',
                               'payload': resource,
                               'target': 'you',
                               'quantity': self.mana[player][resource]})
            else:
                p.sendMessage({'message': 'set',
                               'payload': resource,
                               'target': 'enemy',
                               'quantity': self.mana[player][resource]})
        return

    def _drawCard(self, player, quantity=1):
        for i in range(quantity):
            card = self.decks[player].pop()
            self.hands[player].append(card)
            size = str(len(self.hands[player]) - 1)
            player.sendMessage({'message': 'set',
                                'payload': 'hand'+size,
                                'target': 'you',
                                'quantity': card})

    def _playCard(self, player, card, location=None):
        # Card must be in hand...
        if card not in self.hands[player]:
            raise InvalidPlay()
        card_from_hand = self.decks[player].remove(card)
        # Retrieve the full object
        card_object = get_card(card_from_hand)
        # Get Card type (Hero, Land, Enchantment, Spell)
        if card_object.getType() != 'Land':
            # Cost, Color
            cost = card_object.getCost()
            color = card_object.getColor()
            # Do I have Resources?
            if not self._consumeResource(player, color, cost):
                raise InvalidPlay()
            # Play the actual card...
            if card_object.getType() == 'Hero':
                if not location:
                    raise InvalidPlay()
                # Trash Hero if someone else is there, otherwise just play it
                if self.creatures[player][location]:
                    self.gys[player].insert(0, self.creatures[player][location])
                self.creatures[player][location] = card_object
                return
            if card_object.getType() == 'Enchantment':
                # Has to be placed on someone
                if not location:
                    raise InvalidPlay()
                return
            if card_object.getType() == 'Spell':
                return
        # We have a Land
        else:
            # If we have a land into play, trash it and put this new one
            if len(self.lands[player]) > 0:
                self.gys[player].insert(0, self.lands[player].pop())
            self.lands[player] = card_object

    def _playCombat(self):
        pass

    def matchGlobalStatus(self):
        pass

    def matchVisibleStatus(self, player):
        pass

    def concedeGame(self, who):
        pass

    def endGame(self):
        pass

    def endPhase(self):
        pass

    def endTurn(self, player):
        # It's not your turn!
        if player is not self.current_player:
            raise InvalidPlay()
        for p in self.players:
            if p is not player:
                self.current_player = p
            else:
                self.other_player = p
            p.sendMessage({'message': 'pass', 'payload': ''})

    def makePlay(self, player, message):
        # It's not your turn!
        if player is not self.current_player:
            raise InvalidPlay()
        # This two must always exist
        payload = message['payload']
        target = message['target']
        data = payload.split('.')
        if len(data) > 1:
            resource = data[1]
            quantity = int(message['quantity'])
            self._increaseResource(target, resource, quantity)
        elif data[0] == 'draw':
            quantity = int(message['quantity'])
            self._drawCard(player, quantity)
        return

    def getId(self):
        return self.match_id

    def joinPlayer(self, player):
        self.players.append(player)
        self.decks[player] = random.sample(range(250),40)
        self.gys[player] = []
        self.lands[player] = []
        self.creatures[player] = [None, None, None]
        self.hands[player] = []
        self.mana[player] = {'hp': 25, 'gen': 0, 'cha': 0, 'int': 0, 'agi': 0, 'con': 0, 'str': 0}
        self.hp = {player: self.match_settings['players_hp']}

    def leave(self, player_leaving):
        for player in self.players:
            if not player is player_leaving and player is not None:
                player.sendMessage({'message': 'parted', 'payload': ''})
        # Fire some event to countdown until total removal from game

    def joinSpectator(self, spectator):
        self.spectators.append(spectator)