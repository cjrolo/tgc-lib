from tornado import websocket
import json

from server import players_online, available_games, existing_games
from player import Player

players_sockets = {}

valid_messages = ['search', 'create', 'start', 'concede', 'pass', 'play']

def validateMessage(json_message):
    message = json.loads(json_message)
    try:
        if message["message"] not in valid_messages:
            return False
    except KeyError:
            return False
    return True

def parseMessage(json_message, player):
    message = json.loads(json_message)
    function_mapping = {'create': available_games.searchGame,
                        'search': available_games.searchGame,
                        'start': None,
                        'concede': existing_games.endGame,
                        'pass': existing_games.endTurn,
                        'play': existing_games.makePlay,
                        }
    try:
        # I hate hardcoded stuff... But it has to be
        if message["message"] == 'play':
            function_mapping[message["message"]](player, message)
        else:
            function_mapping[message["message"]](player)
        return True
    except KeyError:
        return False

class GameCommunication(websocket.WebSocketHandler):
    def open(self):
        user_json = json.loads(self.get_secure_cookie("auth_user").decode("utf-8"))
        if not user_json:
            self.close()
        player = Player(user_json['email'], self)
        players_online.addPlayer(player)
        players_sockets[self] = player
        print(player)

    def on_message(self, message):
        user_json = json.loads(self.get_secure_cookie("auth_user").decode("utf-8"))
        if not user_json:
            self.close()
        if not validateMessage(message):
            self.close()
        print("Got Message: " + message)
        player = players_sockets[self]
        parseMessage(message, player)

    def on_close(self):
        user_json = json.loads(self.get_secure_cookie("auth_user").decode("utf-8"))
        if not user_json:
            self.close()
        player = players_sockets[self]
        del players_sockets[self]
        player.Offline()
        print("WebSocket closed, game ended!")