__author__ = 'CarlosJR'
from abc import ABCMeta, abstractmethod


class BaseMechanic(metaclass=ABCMeta):
    """
    The ideia is getting a tupple of targets and effects.
    Some mechanics need data (increasing HP by X, remove Y Attack),
    others no (Destroy card).
    """
    @staticmethod
    def get_mechanics():
        return {x.__name__: x for x in BaseMechanic.__subclasses__()}


class ChangeStat(BaseMechanic):

    def __init__(self, target, data):
        self.target = target
        self.data = data

    def apply(self):
        pass


class DiscardCardFromHand(BaseMechanic):

    def __init__(self, target, data):
        self.target = target
        self.data = data

    def apply(self):
        pass


def apply_mechanic(target, mechanic, **data):
    mechanic_class = BaseMechanic.get_mechanics().get(mechanic, False)
    if mechanic_class:
        mechanic_object = mechanic_class(target, data)
        return mechanic_object.apply()
    return False