

class GameStateMachine():

    def __init__(self, match_id):
        self.match_id = match_id
        self.match_progress = {}
        self.turn_number = 0
        self.stateStatus = {}

    def endTurn(self):
        self.turn_number += 1
        self.match_progress[self.turn_number] = self.stateStatus

