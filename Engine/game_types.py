__author__ = 'cjrolo'

# resource types
GENERIC = 'gen'
CHARISMA = 'cha'
INTELLIGENCE = 'int'
AGILITY = 'agi'
CONSTITUTION = 'con'
STRENGTH = 'str'
# That's a resource also... without it you die!
HIT_POINTS = 'hp'
mana_types = [GENERIC, CHARISMA, INTELLIGENCE, AGILITY, CONSTITUTION, STRENGTH, HIT_POINTS]

# Card Types
BATTLEFIELD = 'Battlefield'
BATTLE_PLAN = 'Battle Plan'
HERO = 'Hero'
EQUIPMENT = 'Equipment'
card_types = [BATTLEFIELD, BATTLE_PLAN, HERO, EQUIPMENT]
