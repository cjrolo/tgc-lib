import copy
"""
File that has the State of the Board and all the variables in it
"""

class Board():
    def __init__(self, deckA, deckB):
        self.deckA = copy.copy(deckA)
        self.deckB = copy.copy(deckB)
        self.gyA = self.gyB = []
        self.landA = self.landB = None
        self.creaturesA = self.creaturesB = [None, None, None]
        self.handA = self.handB = [None, None, None, None, None]
        self.manaA = self.manaB = dict()