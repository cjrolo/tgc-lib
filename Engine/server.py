__author__ = 'CarlosJR'

from game import Game
from player import Player
from random import choice

available_games_list = []
existing_games_list = []
players_online_list = {}


class OnlinePlayers():

    def listPlayers(self):
        pass

    def addPlayer(self, player):
        players_online_list[player.id] = player

    def removePlayer(self, player):
        del players_online_list[player.id]

    def isPlayerOnline(self, player):
        if player.id in players_online_list.keys():
            return True
        return False

    def getPlayer(self, player_id):
        try:
            return players_online_list[player_id]
        except:
            return None


class AvailableGames():
    """
    All games than have 1 player and are waiting for another
    """
    def createGame(self, player):
        # Deprecate this soon... doesn't make sense, use matchmaking always
        new_game = Game(player)
        available_games_list.append(new_game)
        return

    def isAvailable(self):
        if len(available_games_list) > 0:
            return True
        return False

    def searchGame(self, player):
        if not self.isAvailable():
            self.createGame(player)
            return
        # Choose Random game
        game = choice(available_games_list)
        available_games_list.remove(game)
        game.joinPlayer(player)
        self.startGame(game)

    def startGame(self, game):
        """
        Move a game from the Available list to the In Progress
        """
        print("Starting game %s" % game.match_id)
        existing_games_list.append(game)
        game.start()


class GamesInProgress():
    """
    All games than have 2 players
    """
    def inGame(self, player):
        """
        Checks if this player is in any game in process
        (Useful for D/C etc...
        :param player:
        :return:
        """
        pass

    def makePlay(self, player, message):
        # Get the game for this player
        game = player.getGame()
        # Make the play
        if game:
            game.makePlay(player, message)
        game.gameState()

    def endTurn(self, player):
        # Get the game for this player
        game = player.getGame()
        # Make the play
        if game:
            game.endTurn(player)

    def endGame(self, player):
        pass

    def getGame(self, player):
        pass


players_online = OnlinePlayers()
available_games = AvailableGames()
existing_games = GamesInProgress()