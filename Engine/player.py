__author__ = 'CarlosJR'
import json

class Player():

    def __init__(self, id, socket=None):
        self.id = id
        if socket:
            self.socket = socket
        self.nickname = id
        self.name = id
        self.game = None

    def setSocket(self, socket):
        self.socket = socket

    def setName(self, name):
        self.name = name

    def setGame(self, game):
        # Should weakref this?
        self.game = game

    def getGame(self):
        return self.game

    def setNickname(self, nickname):
        self.nickname = nickname

    def sendMessage(self, message):
        if self.socket:
            self.socket.write_message(json.dumps(message))

    def Offline(self):
        self.socket = None
        if self.game:
            self.game.leave(self)

    def __repr__(self):
        output = "%s (%s)" % (self.nickname, self.id)
        return output