/**
 * Created with PyCharm.
 * User: CarlosJR
 * Date: 03-04-2013
 * Time: 19:43
 * To change this template use File | Settings | File Templates.
*/
/* On document load! Let's go! */
$(document).ready(function() {
    set_status_message('Searching for opponent...', 'info');
    var ws = new WebSocket("ws://localhost:8081/gamews");
    var comm = new GameCommunication(ws);
    ws.onopen = function(evt){
        comm.joinGame();
    }
    ws.onmessage = function (evt) {
        parse_message(evt.data)
        var dados = JSON.parse(evt.data);
        if(dados.message == 'start'){
            set_status_message('Player connected! Game starting!', 'success');
            if (dados.payload == 0){
                game_status = true;
                set_status_message('Your turn!', 'success');
            } else {
                set_status_message('Waiting for your turn...', 'info');
            }
        }
        if(dados.message == 'parted'){
            game_status = false;
            set_status_message('Opponent offline! Waiting for reconnect...', 'error');
        }
    };
    /* Game functionality */
    $(".icon-minus").click(function () {
        if (game_status){
            var x = $(this).parent().parent().attr('id');
            var message = {message: "play",
                           payload: 'resource.'+x,
                           target: 'you',
                           quantity: '1'}
            send_message(ws, message);
        }
    });
    $(".icon-plus").click(function () {
        if (game_status){
            var x = $(this).parent().parent().attr('id');
            var message = {message: "play",
                payload: 'resource.'+x,
                target: 'you',
                quantity: '1'}
            send_message(ws, message);
        }
    });
    $("#end-turn").click(function () {
        if (game_status){
            var message = {message: "pass",
                payload: ''}
            send_message(ws, message);
        }
    });
    $("#draw-card").click(function () {
        if (game_status){
            var message = {message: "play",
                           payload: 'draw',
                           target: 'you',
                           quantity: '1'}
            send_message(ws, message);
        }
    });
});