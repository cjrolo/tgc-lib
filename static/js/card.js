 $(document).ready(function() {
    $('.card-container').mouseover(function(){
        $(this).children('.card-actions').show();
    })
    $('.card-container').mouseout(function(){
        $(this).children('.card-actions').hide();
    })
    $('.card-destroy').click(function(){
        var card_id = $(this).attr('data-content');
        var card_type = $(this).attr('data-target');
        var card = $('#'+card_id);
        $.ajax({
            url: "/ajax/"+card_type+"/delete/"+card_id,
            context: ''
        }).done(function(res) {
                card.remove();
                if ($('.card-container').length == 0){
                    document.location.reload();
                }
            });
    })
 })
