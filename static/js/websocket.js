/**
 * Created with PyCharm.
 * User: CarlosJR
 * Date: 03-04-2013
 * Time: 19:43
 * To change this template use File | Settings | File Templates.
 */
game_status = false;

function set_status_message(message, type){
    $('#info_holder').html("<div class=\"ink-alert "+type+"\">"+
        "<button class=\"ink-close\">&times;</button>"+
        "<p>"+message+"</p>"+
        "</div>");
}

function send_message(ws, data){
    ws.send(JSON.stringify(data));
}
function parse_message(data){
    var dados = JSON.parse(data);
    console.log(data)
    switch(dados.message){
        case 'set':
            switch(dados.target)
            {
                case 'you':
                    $('#y-'+dados.payload).html(dados.quantity);
                    break;
                case 'enemy':
                    $('#e-'+dados.payload).html(dados.quantity);
                    break;
                case 'all_players':
                    $('#e-'+dados.payload).html(dados.quantity);
                    $('#y-'+dados.payload).html(dados.quantity);
                    break;
                default:
                    return
            }
            break;
        case 'pass':
            game_status = !game_status
            if (game_status){
                set_status_message('Your turn!', 'success');
            } else {
                set_status_message('Waiting for your turn...', 'info');
            }
            break;
    }
    return;
}

function GameCommunication(web_socket){
    this.web_socket = web_socket;

    this.joinGame = function joinGame(){
        var message = {message: "search", payload: ''};
        send_message(this.web_socket, message);
    }

    this.createGame = function createGame(){
        var message = {message: "create", payload: ''};
        send_message(this.web_socket, message);
    }
}
