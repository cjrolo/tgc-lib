import bottle, os, sys, inspect, json
from bottle import request
from bottle import jinja2_template as template
from bottle import static_file
from tornado.web import decode_signed_value

# So we can import  from other directories
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '.')) + '/Controllers')
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '.')) + '/Models')

# Templates
bottle.TEMPLATE_PATH.insert(0, os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]))) + '/templates/')

from cardGenerator import generate_collection
from heroController import heroController
from landController import landController
import cardsModel, forms

bottle.debug(True)
bottle_app = bottle.Bottle()

def get_cookie_data(request):
    cookie_data = {}
    cookie = request.get_cookie("auth_user")
    if cookie:
        cookie_data = decode_signed_value("ThisIsAStrangeValueForAGeneratedRandomKey!99881297361?!",
                                          "auth_user",
                                          cookie)
        cookie_data = json.loads(cookie_data.decode("utf-8"))
    return cookie_data

@bottle_app.route('/static/<path:path>')
def server_static(path):
    return static_file(path, root=os.path.dirname(__file__))

@bottle_app.route('/')
def index_page():
    return template('index', data=get_cookie_data(request))

@bottle_app.route('/bootstrap_db')
def bootstrap_db():
    import tables
    tables.bootstrap_database()
    return 'Success!'

@bottle_app.route('/ajax/<type>/delete/<identifier>')
def deleteHero(type, identifier):
    classInstanceName = type + 'Controller'
    module = __import__(classInstanceName)
    class_instance = getattr(module, classInstanceName)
    getattr(class_instance, 'delete')(identifier)
    return

@bottle_app.route('/ajax/<type>/edit/<identifier>')
def deleteHero(type, identifier):
    class_instance_name = type + 'Controller'
    module = __import__(class_instance_name)
    class_instance = getattr(module, class_instance_name)
    card = getattr(class_instance, 'getbyId')(identifier)
    form = getattr(forms, 'New'+type.capitalize())(obj=card)
    return template('new_card_form.tpl',
                    form=form,
                    submit='%s/%s' % ('update', type),
                    card_type=type,
                    data=get_cookie_data(request))

@bottle_app.post('/app/forms/update/<type>')
def updateForm(type):
    type = type.capitalize()
    module_name = 'New' + type
    classInstance = getattr(forms, module_name)
    form = classInstance(request.POST.decode())
    validation = form.validate()
    if request.method == 'POST' and validation:
        module_name = type.lower() + 'Controller'
        module = __import__(module_name)
        classInstance = getattr(module, module_name)
        getattr(classInstance, 'update')(key=request.POST.decode().name, **request.POST.decode())
        return template('new_card_form.tpl',
                        form=form,
                        submit='%s/%s' % ('update', type),
                        card_type=type,
                        success=True,
                        data=get_cookie_data(request))
    if request.method == 'POST' and not validation:
        return template('new_card_form.tpl',
                        form=form,
                        submit='%s/%s' % ('update', type),
                        card_type=type,
                        fail=True,
                        data=get_cookie_data(request))
    return template('new_card_form.tpl',
                    form=form,
                    submit='%s/%s' % ('update', type),
                    card_type=type,
                    data=get_cookie_data(request))


@bottle_app.post('/app/forms/<controller>/<action>')
@bottle_app.get('/app/forms/<controller>/<action>')
def drawForm(controller, action):
    module_name = controller + action
    classInstance = getattr(forms, module_name)
    form = classInstance(request.POST.decode())
    validation = form.validate()
    if request.method == 'POST' and validation:
        module_name = action.lower() + 'Controller'
        module = __import__(module_name)
        classInstance = getattr(module, module_name)
        success = getattr(classInstance, controller.lower())(**request.POST.decode())
        return template('new_card_form.tpl',
                        form=form,
                        submit='%s/%s' % (controller, action),
                        card_type=action,
                        success=success,
                        data=get_cookie_data(request))
    if request.method == 'POST' and not validation:
        return template('new_card_form.tpl',
                        form=form,
                        submit='%s/%s' % (controller, action),
                        card_type=action,
                        success=False,
                        data=get_cookie_data(request))
    return template('new_card_form.tpl',
                    form=form,
                    submit='%s/%s' % (controller, action),
                    card_type=action,
                    data=get_cookie_data(request))

@bottle_app.route('/app/<controller>/<action>/<data:path>', method='GET')
def goToRoute(controller, action, data):
    module_name = controller + 'Controller'
    template_name = '%s_%s.tpl' % (controller, action)
    try:
        with open(bottle.TEMPLATE_PATH[0]+template_name): pass
    except IOError:
        template_name = 'card_display.tpl'
    module = __import__(module_name)
    classInstance = getattr(module, module_name)
    method = getattr(classInstance, action)
    signature = inspect.getfullargspec(method)
    if len(signature) > 0:
        if signature.varkw:
            para = data.split('/')
            data_dict = dict(zip(para[0:][::2], para[1:][::2]))
            deck = method(**data_dict)
        else:
            deck = method(data)
    else:
        deck = method()
    if isinstance(deck, list):
        return template(template_name,
                        deck=deck,
                        data=get_cookie_data(request))
    return template(template_name,
                    deck=[deck],
                    data=get_cookie_data(request))

@bottle_app.route('/collection/statistics/<name>')
def getCollectionStatistics(name):
    collection_data = cardsModel.collection_stats()
    return template('statistics.tpl',
                    collection_data=collection_data,
                    data=get_cookie_data(request))

@bottle_app.route('/game')
def gameLobby():
    return template('game_index.tpl', data=get_cookie_data(request))

@bottle_app.route('/game/<type>')
def goToGame(type):
    return template('play.tpl', data=get_cookie_data(request), game={'type': type})