
    <!-- width: 300px; height: 423px; -->
    <div class="card-container" id="{{ card.id }}">
        <div class="card-actions">
            <a class="ink-button card-add" href="#">Add</a>
            <a class="ink-button card-edit" href="/ajax/hero/edit/{{ card.id }}">Edit</a>
            <a class="ink-button card-destroy" data-target="hero" href="#" data-content="{{ card.id }}">Destroy</a>
        </div>
        <div class="card-inside card-{{ card.color }}">
            <div class="card_header">
                <span class="card_name">{{ card.name[0:20] }}</span>
                <span class="card_symbol_{{ card.color }}"> </span>
                <span class="card_cost">{{ card.cost }}</span>
            </div>
            <div class="card_image">
                teste
            </div>
            <div class="card_info">
                <span class="card_type">Hero</span>
                <span class="card_rarity {{ card.rarity }}"> </span>
            </div>
            <div class="card_info_box">
                <p>{{ card.text }}</p>
                <div class="card_stats">
                   {% if card.actives|length or card.passives|length %}
                        <div class="card_pas_act">
                        {% if card.actives|length %}
                            {% for act in card.actives %}
                             {{ act }},
                            {% endfor %}
                        {% endif %}
                        {% if card.passives|length %}
                            {% for pas in card.passives %}
                                {{ pas }},
                            {% endfor %}
                        {% endif %}
                        </div>
                    {% endif %}
                </div>
                <div class="card_promotion">
                    <span><button class="ink-button info" disabled><i class="icon-large icon-double-angle-up"></i> ({{ card.promotion_cost }})</button> :</span>
                    <span> <i class="icon-plus"></i> {{ card.promotion_attack }} <i class="icon-legal"></i></span>
                    <span>  /  </span>
                    <span> <i class="icon-plus"></i> {{ card.promotion_hp }} <i class="icon-heart"></i></span>
                    {% if card.promotion_actives|length or card.promotion_passives|length %}
                        <ul class="card_pas_act">
                        {% if card.promotion_actives|length %}
                            {% for act in card.promotion_actives %}
                             {{ act }},
                            {% endfor %}
                        {% endif %}
                        {% if card.promotion_passives|length %}
                            {% for pas in card.promotion_passives %}
                                {{ pas }},
                            {% endfor %}
                        {% endif %}
                        </ul>
                    {% endif %}
                </div>
                <div class="card_lore">
                            <span>
                            </span>
                </div>
            </div>
            <div class="card_values">
                <span class="hp">{{ card.hp }} <i class="icon-large icon-heart"></i></span>
                <span class="power">{{ card.attack }} <i class="icon-large icon-legal"></i></span>
            </div>
            </div>
        </div>