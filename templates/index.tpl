<html>
    <head>
        {% block head %}
            <link rel="stylesheet" type="text/css" href="http://cdn.ink.sapo.pt/2.2.1/css/ink-min.css">
            <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
            <!--[if IE]>
            <link rel="stylesheet" href="http://css.ink.sapo.pt/v1/css/ink-ie.css" type="text/css" media="screen" title="no title" charset="utf-8">
            <![endif]-->
            <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
            <script type="text/javascript" src="http://cdn.ink.sapo.pt/2.2.1/js/ink.min.js"></script>
            <script type="text/javascript" src="http://cdn.ink.sapo.pt/2.2.1/js/ink-ui.min.js"></script>
            <script type="text/javascript" src="http://cdn.ink.sapo.pt/2.2.1/js/autoload.js"></script>
            <script type="text/javascript" src="http://cdn.ink.sapo.pt/2.2.1/js/prettify.js"></script>

            <script type="text/javascript" src="/static/static/js/card.js"></script>
            <link rel="stylesheet" href="/static/static/css/print.css" type="text/css" media="print" />
            <link rel="stylesheet" href="/static/static/css/main.css" type="text/css" media="screen">
            <title>Our Card Game</title>
        {% endblock %}
    </head>
    <body>
    <div class="wrapper">
    <div id='topbar'>
    <nav class="ink-navigation ink-grid hide-small hide-medium">
        <ul class="menu horizontal flat black shadowed">
            <li class="ink-title"><a href="/" name="nav-home">SSTG CCG</a></li>
            <li>
                <a href="#" class="vt-p">Heroes</a>
                <ul class="submenu">
                    <li><a href="/app/hero/list/1" class="vt-p">List</a></li>
                    <li><a href="/app/forms/New/Hero" class="vt-p">Create</a></li>
                    <li><a href="/app/hero/createRandom/3" class="vt-p">Generate Random</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Battle Plans</a>
                <ul class="submenu">
                    <li><a href="/app/spell/list/all">List</a></li>
                    <li><a href="/app/forms/New/Spell">Create</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Battlefields</a>
                <ul class="submenu">
                    <li><a href="/app/land/list/1">List</a></li>
                    <li><a href="/app/forms/New/Land">Create</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Equipments</a>
                <ul class="submenu">
                    <li><a href="/app/equipment/list/1">List</a></li>
                    <li><a href="/app/forms/New/Equipment">Create</a></li>
                </ul>
            </li>
            <li>
                <a href="/collection/statistics/1">Statistics</a>
            </li>
            {%- if data.name -%}
             <li>
                <a href="#">Collections</a>
                <ul class="submenu">
                    <li><a href="/app/collection/list/1">List</a></li>
                    <li><a href="/app/forms/New/Collection">Create</a></li>
                </ul>
            </li>
            <li>
                <a href="/game" target="_blank">Play!</a>
            </li>
            <li>
                <a href="/auth/logout">
                    {{ data.name }}
                    <i class="icon-signout"></i>
                </a>
            </li>
            {% else%}
            <li>
                <a href="/auth/login">
                    Login
                    <i class="icon-signin"></i>
                </a>
            </li>
            {% endif %}
        </ul>
     </nav>
    </div>
    <br/>
     <div class="ink-container">
     <content>
         {% block content %}
            <div style="margin: 0 auto; width: 350px">
                <img src="/static/static/imgs/imagine.jpg" />
            </div>
         {% endblock %}
     </content>
     </div>
     <br/>
     <footer>
        {% block footer %}{% endblock %}
     </footer>
    </div>
    </body>
</html>
