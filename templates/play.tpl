{% extends "game_index.tpl" %}
{% block content %}
        <div class="ink-row ink-vspace"> <!-- div.ink-row is used to group several "columns" together. -->
            <div class="ink-l70 ink-m70">
                <div class="ink-gutter">
                    <h3>Field</h3>
                    <nav class="ink-navigation">
                        <ul class="menu horizontal grey">
                            <li id='draw-card'>
                                <a href="#">Draw Card</a>
                            </li>
                            <li>
                                <a href="#">End Phase</a>
                            </li>
                            <li id="end-turn">
                                <a href="javascript: void(0);">End Turn</a>
                            </li>
                            <li>
                                <a href="#">Concede Game</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="ink-gutter">
                <div class="ink-row ink-vspace">
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class='card-turned' id='e-hand0'></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class='card-turned' id='e-hand1'></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class='card-turned' id='e-hand2'></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class='card-turned' id='e-hand3'></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class='card-turned' id='e-hand4'></div>
                        </div>
                    </div>
                </div>
                <div class="ink-row ink-vspace">
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class=card-placeholder></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class=card-placeholder></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class=card-placeholder></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <hr/>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class=card-placeholder></div>
                        </div>
                    </div>
                </div>
                <div class="ink-row ink-vspace">
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                </div>
                <div class="ink-l100 ink-m100">
                    <hr/>
                </div>
                    <!-- Player Starts Here -->
                <div class="ink-row ink-vspace">
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <p>20%</p>
                        </div>
                    </div>
                </div>
                <div class="ink-row ink-vspace">
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter">
                            <div class=card-placeholder></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class=card-placeholder></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class=card-placeholder></div>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <hr/>
                        </div>
                    </div>
                    <div class="ink-l20 ink-m20">
                        <div class="ink-gutter"> <!-- div.ink-gutter is used to add the gutter to the column -->
                            <div class=card-placeholder></div>
                        </div>
                    </div>
                </div>
                    <!-- Mão do jogador -->
                <div class="ink-row ink-vspace">
                    <div class="ink-l100 ink-m100">
                        <div class="ink-gutter" id='your-hand'>
                            <div class='card-player_hand' id='y-hand0'></div>
                            <div class='card-player_hand' id='y-hand1'></div>
                            <div class='card-player_hand' id='y-hand2'></div>
                            <div class='card-player_hand' id='y-hand3'></div>
                            <div class='card-player_hand' id='y-hand4'></div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
               <div class="ink-l30 ink-m30">
                   <div class="ink-gutter">
                       <table class="ink-table">
                           <thead>
                               <tr>
                                   <th> </th>
                                   <th>You</th>
                                   <th>Enemy</th>
                               </tr>
                           </thead>
                           <tbody>
                               <tr>
                                   <th><i class="icon-heart icon-medium"></i></th>
                                   <td id="y-hp">0</td>
                                   <td id="e-hp">0</td>
                               </tr>
                               <tr>
                                   <th>Generic</th>
                                   <td id="y-gen">0</td>
                                   <td id="e-gen">0</td>
                               </tr>
                               <tr>
                                   <th>Cha</th>
                                   <td id="y-cha">0</td>
                                   <td id="e-cha">0</td>
                               </tr>
                               <tr>
                                   <th>Int</th>
                                   <td id="y-int">0</td>
                                   <td id="e-int">0</td>
                               </tr>
                               <tr>
                                   <th>Agi</th>
                                   <td id="y-agi">0</td>
                                   <td id="e-agi">0</td>
                               </tr>
                               <tr>
                                   <th>Con</th>
                                   <td id="y-con">0</td>
                                   <td id="e-con">0</td>
                               </tr>
                               <tr>
                                   <th>Str</th>
                                   <td id="y-str">0</td>
                                   <td id="e-str">0</td>
                               </tr>
                           </tbody>
                       </table>
                       <br/>
                       <table class="ink-table">
                           <tbody>
                               <tr id="hp">
                                   <td><i class="icon-heart icon-medium"></i></td>
                                   <td><i class="icon-minus icon-medium"></i></td>
                                   <td><i class="icon-plus icon-medium"></i></td>
                               </tr>
                               <tr id="gen">
                                   <td>Generic</td>
                                   <td><i class="icon-minus icon-medium"></i></td>
                                   <td><i class="icon-plus icon-medium"></i></td>
                               </tr>
                               <tr id="cha">
                                   <td>Cha</td>
                                   <td><i class="icon-minus icon-medium"></i></td>
                                   <td><i class="icon-plus icon-medium"></i></td>
                               </tr>
                               <tr id="int">
                                   <td>Int</td>
                                   <td><i class="icon-minus icon-medium"></i></td>
                                   <td><i class="icon-plus icon-medium"></i></td>
                               </tr>
                               <tr id="agi">
                                   <td>Agi</td>
                                   <td><i class="icon-minus icon-medium"></i></td>
                                   <td><i class="icon-plus icon-medium"></i></td>
                               </tr>
                               <tr id="con">
                                   <td>Con</td>
                                   <td><i class="icon-minus icon-medium"></i></td>
                                   <td><i class="icon-plus icon-medium"></i></td>
                               </tr>
                               <tr id="str">
                                   <td>Str</td>
                                   <td><i class="icon-minus icon-medium"></i></td>
                                   <td><i class="icon-plus icon-medium"></i></td>
                               </tr>
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>
       <script type="text/javascript" src="/static/static/js/game.js"></script>
   {% endblock %}