<!DOCTYPE html>
<html>
<head>
    <title>Game Board</title>
    <!-- Stylesheets -->
    <link rel="stylesheet" href="http://css.ink.sapo.pt/v1/css/ink.css">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <!--[if IE]>
    <link rel="stylesheet" href="http://css.ink.sapo.pt/v1/css/ink-ie.css" type="text/css" media="screen" title="no title" charset="utf-8">
    <![endif]-->
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="http://js.sapo.pt/Bundles/Ink-v1.js"></script>

    <script type="text/javascript" src="/static/static/js/card.js"></script>
    <script type="text/javascript" src="/static/static/js/websocket.js"></script>
    <link href="/static/static/css/teste.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id='topbar'>
    <nav class="ink-navigation ink-collapsible ink-container">
        <ul class="menu horizontal black">
            <li class="ink-title"><a href="/" name="nav-home">SSTG CCG</a></li>
            <li>
                <a href="/game/create">Create</a>
            </li>
            <li>
                <a href="/game/join">Match Making</a>
            </li>
            <li>
                <a href="/game/duel">Duel</a>
            </li>
            {%- if data.name -%}
                <li>
                    <a href="/auth/logout">
                        {{ data.name }}
                        <i class="icon-signout"></i>
                    </a>
                </li>
            {% else%}
                <li>
                    <a href="/auth/login">
                        Login
                        <i class="icon-signin"></i>
                    </a>
                </li>
            {% endif %}
        </ul>
    </nav>
</div>
<br/>
<div class="ink-container">
    <div class="ink-l100 ink-m100 ink-s100">
        <div id="info_holder">
 <!--           <div class="ink-alert info">
                <button class="ink-close">&times;</button>
                <i class="icon-spinner icon-spin"></i> Looking for players...
            </div> -->
        </div>
{% block content %}
{% endblock %}
    </div>
</div>
 <br/>
 <footer>
    {% block footer %}{% endblock %}
 </footer>
</body>
</html>