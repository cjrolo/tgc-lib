
    <!-- width: 300px; height: 423px; -->
    <div class="collection-container" id="{{ collection.id }}">
        <p class="ink-alert warning">
            {{ collection.name }}
            {{ collection.type }}
                <a class="ink-button card-edit info" href="/ajax/collection/edit/{{ collection.id }}" >Edit</a>
                <a class="ink-button card-destroy caution" data-target="collection" href="#" data-content="{{ collection.id  }}">Delete</a>

        </p>
    </div>