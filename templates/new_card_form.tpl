{% from "_formshelper.html" import form_fields, action_buttons  %}
{% extends "index.tpl" %}
{% block content %}
    {% if success %}
        <div class="ink-alert success">
            <button class="ink-close">&times;</button>
            <p><b>Success:</b> Your card was created!</p>
        </div>
        <br/>
    {% endif %}
    {% if success == False %}
        <div class="ink-alert error">
            <button class="ink-close">&times;</button>
            <p><b>Error:</b> There was an error creating the card!</p>
        </div>
        <br/>
    {% endif %}
    <form method="POST" action="/app/forms/{{ submit }}" class="ink-form block">
        {{ form_fields(form, None, card_type) }}
        {{ action_buttons('Submit') }}
    </form>
{% endblock %}