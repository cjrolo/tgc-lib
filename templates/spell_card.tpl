
    <!-- width: 300px; height: 423px; -->
    <div class="card-container" id="{{ card.name }}">
        <div class="card-actions">
            <a class="ink-button card-add" href="#">Add</a>
            <a class="ink-button card-edit" href="/ajax/spell/edit/{{ card.name }}" >Edit</a>
            <a class="ink-button card-destroy" data-target="spell" href="#" data-content="{{ card.name }}">Destroy</a>
        </div>
        <div class="card-inside card-{{ card.color }}">
            <div class="card_header">
                <span class="card_name">{{ card.name[0:20] }}</span>
                <span class="card_symbol_{{ card.color }}"> </span>
                <span class="card_cost">{{ card.cost }}</span>
            </div>
            <div class="card_image">
                teste
            </div>
            <div class="card_info">
                <span class="card_type">{{ card.cast_type }}</span>
                <span class="card_rarity {{ card.rarity }}"> </span>
            </div>
            <div class="card_info_box">
            <div class="card_text">
                {{ card.text }}
            </div>
             <br/>
            {% if card.lore %}
                <div class="card_lore note">
                    <i>{{ card.lore }}</i>
                </div>
            {% endif %}
            </div>
            </div>
        </div>