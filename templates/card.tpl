
    <!-- width: 300px; height: 423px; -->
    <div class="card-container" id="{{ card.name }}">
        <div class="card-actions">
            <a class="ink-button card-add" href="#">Add</a>
            <a class="ink-button card-edit" href="/ajax/hero/edit/{{ card.name }}">Edit</a>
            <a class="ink-button card-destroy" data-target="hero" href="#" data-content="{{ card.name }}">Destroy</a>
        </div>
        <div class="card-inside card-{{ card.card_type }}">
            <div class="card_header">
                <span class="card_name">{{ card.name[0:20] }}</span>
                <span class="card_symbol_{{ card.card_type }}"> </span>
                <span class="card_cost">{{ card.cost }}</span>
            </div>
            <div class="card_image">
                teste
            </div>
            <div class="card_info">
                <span class="card_type">Hero</span>
                <span class="card_rarity {{ card.rarity }}"> </span>
            </div>
            <div class="card_info_box">
                <div class="card_stats">
                   {% if card.active|length or card.passive|length %}
                        <ul class="card_pas_act">
                        {% if card.active|length %}
                             <li class='icon-eye-open icon-small'>
                            {% for act in card.active %}
                                 {{ act }},
                            {% endfor %}
                            </li>
                        {% endif %}
                        {% if card.passive|length %}
                            <li class="icon-leaf icon-small">
                            {% for act in card.passive %}
                                 {{ act }},
                            {% endfor %}
                            </li>
                        {% endif %}
                        </ul>
                    {% endif %}
                </div>
                <div class="card_promotion">
                    <span><button class="ink-button info" disabled><i class="icon-large icon-double-angle-up"></i> ({{ card.promotion['cost'] }})</button> :</span>
                    <span> <i class="icon-plus"></i> {{ card.promotion['attack'] }} <i class="icon-legal"></i></span>
                    <span>  /  </span>
                    <span> <i class="icon-plus"></i> {{ card.promotion['hp'] }} <i class="icon-heart"></i></span>
                    {% if card.promotion['active']|length or card.promotion['passive']|length %}
                        <ul class="card_pas_act">
                        {% if card.promotion['active']|length %}
                             <li class='icon-eye-open icon-small'>
                            {% for act in card.promotion['active'] %}
                                 {{ act }},
                            {% endfor %}
                            </li>
                        {% endif %}
                        {% if card.promotion['passive']|length %}
                            <li class="icon-leaf icon-small">
                            {% for act in card.promotion['passive'] %}
                                 {{ act }},
                            {% endfor %}
                            </li>
                        {% endif %}
                        </ul>
                    {% endif %}
                </div>
                <div class="card_lore">
                            <span>
                            </span>
                </div>
            </div>
            <div class="card_values">
                <span class="hp">{{ card.hp }} <i class="icon-large icon-heart"></i></span>
                <span class="power">{{ card.attack }} <i class="icon-large icon-legal"></i></span>
            </div>
            </div>
        </div>