{% extends "index.tpl" %}
{% block content %}
<div class="ink-l100 ink-m100 ink-s100">
    {% for card in deck %}
    <div class="ink-l33 ink-m33 ink-s33">
        {% include "hero_card.tpl" %}
    </div>
    {% endfor %}
</div>
{% endblock %}

{% block footer %}
{% endblock %}
