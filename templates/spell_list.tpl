{% extends "index.tpl" %}
{% block content %}
<div class="ink-l100 ink-m100 ink-s100">
<nav class="ink-navigation ink-dockable">
    <ul class="menu horizontal black"><li class="active">
        <a class="scrollableLink home vt-p" href="#nav-home">
        <i class="icon-chevron-up ink-for-l"></i><span class="ink-for-m ink-for-s">Back to Top</span>
        </a>
        <button class="ink-for-s ink-for-m ink-button" style="display: none;"><i class="icon-reorder" style="height:auto"></i></button></li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/spell/list/all">All</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/spell/list/color/Charisma">Charisma</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/spell/list/color/Intelligence">Intelligence</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/spell/list/color/Agility">Agility</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/spell/list/color/Constitution">Constitution</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/spell/list/color/Strength">Strength</a>
        </li>
    </ul>
</nav>
    <br/>
    {% for card in deck %}
        {% if loop.first %}
            <!--<div class="ink-l100 ink-m100 ink-s100" style="page-break-after:always">-->
        {% endif %}
        {% if loop.index0 is divisibleby 3 %}
            {% if loop.first %}
            <div class="ink-l100 ink-m100 ink-s100">
            {% else %}
            </div>
            <div class="ink-l100 ink-m100 ink-s100">
            {% endif %}
        {% endif %}
        <div class="ink-l33 ink-m33 ink-s33">
            {% include "spell_card.tpl" %}
        </div>
    {% endfor %}
</div>
{% endblock %}
