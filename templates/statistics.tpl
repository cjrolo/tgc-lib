{% extends "index.tpl" %}
{% block content %}
<h3> Heroes Statistics </h3>
<table class="ink-table ink-bordered ink-zebra">
    <thead>
    <tr>
        <th>Color</th>
        <th>Common</th>
        <th>Uncommon</th>
        <th>Rare</th>
        <th>Legendary</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>Charisma</th>
        <th>{{ collection_data['mixed']['Charisma-Common'] }}</th>
        <th>{{ collection_data['mixed']['Charisma-Uncommon'] }}</th>
        <th>{{ collection_data['mixed']['Charisma-Rare'] }}</th>
        <th>0</th>
        <th>{{ collection_data['color']['Charisma'] }}</th>
    </tr>
    <tr>
        <th>Strength</th>
        <th>{{ collection_data['mixed']['Strength-Common'] }}</th>
        <th>{{ collection_data['mixed']['Strength-Uncommon'] }}</th>
        <th>{{ collection_data['mixed']['Strength-Rare'] }}</th>
        <th>0</th>
        <th>{{ collection_data['color']['Strength'] }}</th>
    </tr>
    <tr>
        <th>Agility</th>
        <th>{{ collection_data['mixed']['Agility-Common'] }}</th>
        <th>{{ collection_data['mixed']['Agility-Uncommon'] }}</th>
        <th>{{ collection_data['mixed']['Agility-Rare'] }}</th>
        <th>0</th>
        <th>{{ collection_data['color']['Agility'] }}</th>
    </tr>
    <tr>
        <th>Constitution</th>
        <th>{{ collection_data['mixed']['Constitution-Common'] }}</th>
        <th>{{ collection_data['mixed']['Constitution-Uncommon'] }}</th>
        <th>{{ collection_data['mixed']['Constitution-Rare'] }}</th>
        <th>0</th>
        <th>{{ collection_data['color']['Constitution'] }}</th>
    </tr>
    <tr>
        <th>Intelligence</th>
        <th>{{ collection_data['mixed']['Intelligence-Common'] }}</th>
        <th>{{ collection_data['mixed']['Intelligence-Uncommon'] }}</th>
        <th>{{ collection_data['mixed']['Intelligence-Rare'] }}</th>
        <th>0</th>
        <th>{{ collection_data['color']['Intelligence'] }}</th>
    </tr>
    </tbody>
</table>
{% endblock %}