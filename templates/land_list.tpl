{% extends "index.tpl" %}
{% block content %}
<div class="ink-l100 ink-m100 ink-s100">
<nav class="ink-navigation ink-dockable">
    <ul class="menu horizontal black"><li class="active">
        <a class="scrollableLink home vt-p" href="#nav-home">
        <i class="icon-chevron-up ink-for-l"></i><span class="ink-for-m ink-for-s">Back to Top</span>
        </a>
        <button class="ink-for-s ink-for-m ink-button" style="display: none;"><i class="icon-reorder" style="height:auto"></i></button></li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/land/list/all">All</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/land/list/rarity/Legendary">Legendary</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/land/list/rarity/Rare">Rare</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/land/list/rarity/Uncommon">Uncommon</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/land/list/rarity/Common">Common</a>
        </li>
    </ul>
</nav>
    <br/>
    {% for card in deck %}
        {% if loop.first %}
            <!--<div class="ink-l100 ink-m100 ink-s100" style="page-break-after:always">-->
        {% endif %}
        {% if loop.index0 is divisibleby 3 %}
            {% if loop.first %}
            <div class="ink-l100 ink-m100 ink-s100">
            {% else %}
            </div>
            <div class="ink-l100 ink-m100 ink-s100">
            {% endif %}
        {% endif %}
        <div class="ink-l33 ink-m33 ink-s33">
            {% include "land_card.tpl" %}
        </div>
    {% endfor %}
</div>
{% endblock %}
