{% extends "index.tpl" %}
{% block content %}
<div class="ink-l100 ink-m100 ink-s100">
<nav class="ink-navigation ink-dockable">
    <ul class="menu horizontal black"><li class="active">
        <a class="scrollableLink home vt-p" href="#nav-home">
        <i class="icon-chevron-up ink-for-l"></i><span class="ink-for-m ink-for-s">Back to Top</span>
        </a>
        <button class="ink-for-s ink-for-m ink-button" style="display: none;"><i class="icon-reorder" style="height:auto"></i></button></li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/collection/list/all">All</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/collection/list/type/Deck">Decks</a>
        </li>
        <li style="display: inline-block;">
            <a class="scrollableLink vt-p" href="/app/collection/list/type/Collection">Collection</a>
        </li>
    </ul>
</nav>
    <br/>
    {% for collection in deck %}
        <div class="ink-l100 ink-m100 ink-s100">
            {% include "collection.tpl" %}
        </div>
    {% endfor %}
</div>
{% endblock %}
